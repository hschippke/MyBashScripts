#!/bin/bash
# Shell script to backup to remote HD using rsync
# by Joern Ende http://joern-ende.de
# modified by Henning Schippke

## VARIABLES ##

# Directories to be backed-up. No trailing slash! $HOME is a system variable and points to your home directory. Do not use tilde!
SOURCE="$HOME/Documents"

# Path to remote machine
REMOTE_MACHINE="users"

# Username on remote machine
REMOTE_USER="schippke"

# Destination folder on remote machine
REMOTE_DESTINATION="/mnt/user_disk/backup/$REMOTE_USER/"

# rsync options
RSYNCCONF="--delete --exclude=.* --exclude=Austausch_Win7 --exclude=Autodesk --exclude=Microsoft-Benutzerdaten --exclude=WebEx --exclude=RDC* -avz -e ssh"

### DO NOT EDIT ###

rsync $RSYNCCONF $SOURCE $REMOTE_USER@$REMOTE_MACHINE:$REMOTE_DESTINATION

# rsync --delete --exclude=.* --exclude=Austausch_Win7 --exclude=Autodesk --exclude=Microsoft-Benutzerdaten --exclude=WebEx -avz -e ssh /Users/schippke/Documents schippke@users:/home/schippke/Backups