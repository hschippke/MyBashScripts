#!/bin/bash

# Format aller Bilder eines Ordners mittels convert √§ndern

for pic in ./Bilder_Bearbeitet_tif/*.tif
    do
         picname=$(basename "$pic" .tif)
          newname=$picname.jpg

         echo "Bearbeite Bild:    $picname  --->  $newname"
    
         convert "$pic" "./Bilder_Bearbeitet_jpg/$newname"
     done