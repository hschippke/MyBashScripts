#!/bin/bash

# Zuschneiden von allen Bildern eines Ordners mittels convert

for pic in ./Bilder/*.png
    do
         picname=$(basename "$pic")
         echo "Bearbeite Bild:    $picname"
         convert "$pic" -crop 1680x1680+660+660 "./Bilder_Bearbeitet/$picname"
     done